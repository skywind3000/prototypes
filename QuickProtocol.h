//=====================================================================
// 
// QuickProtocol.h - 协议定制
//
// NOTE:
// 为 QuickNet 安装新的协议
//
//=====================================================================

#include <stddef.h>
#include <stdint.h>
#include <functional>


//---------------------------------------------------------------------
// 协议数据包
//---------------------------------------------------------------------
struct ByteBuffer
{
	// 见 ByteBuffer.h
};



//---------------------------------------------------------------------
// 协议包定义：协议包由二进制缓存包体 bytes 和一系列结构组成
// 这个协议包会在各层协议间上下流动
// 
//---------------------------------------------------------------------
struct QuickPacket
{
	ByteBuffer bytes;
	int32_t cid;
	int32_t sid;
	int protocol;
	// ...
};


//---------------------------------------------------------------------
// 比如链接管理层，会剥离 cid 并处理
//     packet->cid = packet->bytes.pop_head_uint32();
// 验证完链接合法性后又继续读取是第几个协议
//     packet->protocol = packet->bytes.pop_head_uint8();
//
// 然后如果该 protocol 包含 fec 层，又会进一步：
//     connection->proto_fec->input(packet);
// 而在 fec 模块中，可能进一步剥离头部（举例）：
//     packet->fec_size = packet->bytes.pop_head_uint8();
//     packet->fec_parity = packet->bytes.pop_head_uint8();
//     packet->fec_index = packet->bytes.pop_head_uint8();
// 然后 fec 模块就把这个 packet 吞了，负责生命周期管理，至于是否
// 向上层转递，或者直接删除（重复了），或者通过 FEC 运算生成一个
// 新的 packet 给上层，那是 fec 模块的事情了。
//
// 最后经过了 FEC 层了，需把剩下的数据给 kcp 了：
//     connection->proto_kcp->input(packet);
// 然后 kcp 对象就把这个新的 packet 给吞了，至于要不要向上层用户层
// 继续产生新的 packet 那是 kcp 这个协议的实现了。
//---------------------------------------------------------------------


//---------------------------------------------------------------------
// 协议定义，可以安装到 QuickNet 中的新协议
//---------------------------------------------------------------------
class QuickProtocol
{
public:
	virtual ~QuickProtocol();

public:

	// 初始化
	virtual bool Init(uint32_t millisec) = 0;

	// 更新协议状态
	virtual void Update(uint32_t millisec) = 0;

	// 检查下一次更新的时间
	virtual uint32_t CheckTime(uint32_t millisec) = 0;

	// 上层协议发送一个数据包
	virtual int Send(QuickPacket *packet) = 0;

	// 上层协议接受一个数据包
	virtual QuickPacket *Recv() = 0;

	// 下层协议输入一个数据包
	virtual int Input(QuickPacket *packet) = 0;

	// 回调：下层协议需要发送数据包了
	std::function<void(QuickPacket *packet)> Output = NULL;

	// 回调：可以收包了，上层可以调用 Recv 了
	std::function<void()> OnPacketArrival = NULL;

	// 回调：可以发包了
	std::function<void()> OnPacketCanWrite = NULL;
};



