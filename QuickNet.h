//=====================================================================
// 
// QuickNet.h - 主接口文件
//
// NOTE:
//
//=====================================================================
#include <stddef.h>
#include <stdint.h>

#include "QuickProtocol.h"


//---------------------------------------------------------------------
// 全局配置
//---------------------------------------------------------------------
struct QuickConfig
{
	bool can_accept;
};


//---------------------------------------------------------------------
// 按链接配置的
//---------------------------------------------------------------------
enum QuickOption
{
	KEEPALIVE = 1,       // 链接保活的时间
	KCP_INTERVAL = 2,    // 保活时间
	KCP_NODELAY = 3,     // 立即发送
	KCP_NC = 4,          // 关闭流控
	KCP_SNDWND = 5,      // 发送窗口
	KCP_RCVWND = 6,      // 接收窗口
	KCP_MINRTO = 7,      // 最小 RTO
	KCP_STREAM = 8,      // 流模式
	NACK_WINDOW = 9,     // NACK 最大窗口
};


//---------------------------------------------------------------------
// QuickNet
//---------------------------------------------------------------------
class QuickNet
{
public:
	virtual ~QuickNet();
	QuickNet();

public:

	// 初始化：地址列表是逗号分割的 ip:port
	bool Init(const char *address_list, const QuickConfig& config);

	// 销毁
	void Quit();

	// 更新本地地址，重新绑定
	bool Rebind(const char *address_list);


	// 创建链接，返回 cid，小于零代表失败
	// address_list - 逗号分割的 ip:port 地址
	// enc_key - 加密统一用在链接时
	// size - 密钥 size
	int Connect(const char *address_list, const char *enc_key, int size);

	// 断开链接
	// cid - 链接编号
	// code - 可以指明一个数字，对方收到链接断开消息时能够收到
	// 如果时内部错误，比如 keepalive 超时时，code < 0，
	void Disconnect(int cid, int code);

	// 更新远端地址，需要处理好地址列表的增删
	bool UpdateRemoteAddress(const char *address_list);

	// 网络模拟：传入链接 cid，
	// inband - 模拟方向，true 代表输入/接收侧，false 代表输出/发送侧
	// rtt - 模拟延迟，单位毫秒
	// jitter - 模拟波动，单侧数据包最终延迟为：rtt - jitter + random(jitter)
	// lost_rate - 万分之一单位的丢包率
	// band_width - 字节为单位的带宽（包括 UDP/IP 头）
	// 上面 4 个值传入 -1 代表禁用，全部 -1 代表关闭某一侧网络模拟
	void EmulateTraffic(int cid, bool inband, int rtt, 
		int jitter, int lost_rate, int band_width);

	// 配置链接：opt 是配置的项目，value 是新的值
	int Option(int cid, int opt, int value);


	// 创建流，返回流 sid，小于零代表失败
	// cid - 链接编号
	// protocol - 使用协议编号（见后面 ProtocolID）
	// name - 字符串的流名（前期可以不实现）
	int StartStream(int cid, int protocol, const char *name);

	// 停止流
	int StopStream(int cid, int sid);

	// 发送数据
	int Send(int cid, int sid, const void *data, int size);


	// 等待数据，相当于 poll
	bool Wait(int millisec);

	// 唤醒等待
	void Wake();

	// 取得消息：循环调用直到返回 -1
	// 返回值：消息长度，没消息返回 -1，如 max_size < 0，返回待取消息长度
	// event: 消息编号
	// wparam: 第一个参数
	// lparam: 第二个参数
	// data: 接收消息体的数据指针
	// max_size: 缓存最大长度，如果消息超过该值，则接收不成功，返回 -2
	int GetMessage(int &event, int32_t &wparam, int32_t &lparam, void *data, int max_size);
};


//---------------------------------------------------------------------
// 使用方式
//---------------------------------------------------------------------

/*
 * while (not_stop) {
 *     quicknet->Wait(10);  // 最长等待 10ms 
 *     while (1) {  // 持续收消息直到本轮没消息了
 *         int hr = quicknet->GetMessage(evt, wp, lp, data, max_size);
 *         if (hr == -1) break;
 *         // 解析合法消息
 *         switch (event) {
 *         case EVT_CONNECTION_ACCEPT:
 *             quicknet->StartStream(wparam, 1, NULL);
 *             break;
 *         case EVT_CONNECTION_ESTAB:
 *             quicknet->StartStream(wparam, 1, NULL);
 *             break;
 *         case EVT_CONNECTION_CLOSED:
 *             break;
 *         case EVT_STREAM_START:
 *             break;
 *         case EVT_STREAM_STOP:
 *             break;
 *         case EVT_STREAM_DATA:
 *             if (lparam == 0) {  // 如过是 server 就返回 echo
 *                 quicknet->Send(wparam, 1, data, hr);
 *             }
 *             break;
 *         }
 * }
*/


//---------------------------------------------------------------------
// GetMessage 返回的消息类型
//---------------------------------------------------------------------

// 新链接被接受（服务端）：
// event = EVT_CONNECTION_ACCEPT
// wparam = cid
// lparam = remote_cid
// data = 远方的 address_list
// size = strlen(data)
const int EVT_CONNECTION_ACCEPT = 0;

// 成功建立链接（客户端）：
// event = EVT_CONNECTION_ESTAB
// wparam = cid
// lparam = remote_cid
// 注：客户端如果链接失败，会直接收到下面的 CLOSED 消息而没有 ESTAB
const int EVT_CONNECTION_ESTAB = 1;

// 链接断开
// event = EVT_CONNECTION_CLOSED
// wparam = cid
// lparam = code, 断开链接的返回码
// 注：不管是服务端 accept 还是客户端主动 connect 出去的链接，
// 断开的时候都会有统一的 CLOSED 消息，便于外层在收到 CLOSED 的时候
// 统一清除关联对象
const int EVT_CONNECTION_CLOSED = 2;

// 远端流开启：远端调用 StartStream
// event = EVT_STREAM_START
// wparam = cid
// lparam = sid
// data = stream_name
// size = strlen(stream_name)
const int EVT_STREAM_START = 3;

// 远端流关闭：远端调用 StopStream
// event = EVT_STREAM_STOP
// wparam = cid
// lparam = sid
// data = reason
// size = sizeof(reason)
// 补充：如果远端不调用 StopStream 直接 Close 链接，那么
// 你将会直接收到 EVT_CONNECTION_CLOSED 而不是 EVT_STREAM_STOP
const int EVT_STREAM_STOP = 4;

// 接受到新数据
// event = EVT_STREAM_DATA
// wparam = cid
// lparam = sid
// data = payload ptr
// size = payload size
const int EVT_STREAM_DATA = 5;


//---------------------------------------------------------------------
// 可用协议，每个 StartStream 时为 stream 指定
// 协议编号的要点在于写死，如果存在两种协议组合，则占用一个新的协议号
// 一样写死，代码实现，简单点
//---------------------------------------------------------------------
enum ProtocolID
{
	NAKED = 0,         // 裸数据，相当于针对链接的 UDP
	NAKED_FEC1 = 1,    // 裸数据 + FEC1 通道
	NAKED_FEC2 = 2,    // 裸数据 + FEC2 通道

	// 前期支持的协议
	KCP = 3,           // KCP 协议
	KCP_FEC1 = 4,      // KCP 使用 FEC1 通道
	KCP_FEC2 = 5,      // KCP 使用 FEC2 通道
	NACK = 6,          // NACK 协议
	NACK_FEC1 = 7,     // NACK 使用 FEC1 通道
	NACK_FEC2 = 8,     // NACK 使用 FEC2 通道

	// 后面扩展的协议
	STCP = 9,          // 自研 TCP
	STCP_NACK = 10,    // 自研 TCP 搭配 NACK
	STCP_FEC1 = 11,    // 自研 TCP 搭配 FEC1
	STCP_FEC2 = 12,    // 自研 TCP 搭配 FEC2

	// 继续扩展第二个 TCP 实现
	TCP2 = 13,         // 自研 TCP 2 号协议
	TCP2_FEC1 = 14,    // 自研 TCP 2 号协议 + FEC1 通道
	TCP2_FEC2 = 15,    // 自研 TCP 2 号协议 + FEC2 通道

	// 增加协议是很轻松的事情，以后可能隔一段时间又会多一种新协议
};



